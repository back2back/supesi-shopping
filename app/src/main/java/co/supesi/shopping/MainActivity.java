package co.supesi.shopping;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.shopping.adapter.StoreCollectionAdapter;
import co.supesi.shopping.model.ApiAppCategoriesResponse;
import co.supesi.shopping.model.ApiUserResponse;
import co.supesi.shopping.model.AppCategory;
import co.supesi.shopping.model.GetGeocodedPlace;
import co.supesi.shopping.model.User;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;
import co.supesi.shopping.ui.ChatActivity;
import co.supesi.shopping.ui.UserViewModel;
import co.supesi.shopping.ui.orders.OrderActivity;
import co.supesi.shopping.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{


    private AppBarConfiguration mAppBarConfiguration;
    StoreCollectionAdapter storeCollectionAdapter;

    public final static String LAT ="lat";
    public final static String LNG = "lng";
    private  final String TAG = "MainActivity";
    public Double lat;
    public Double lng;

    int clickedItem;
    private String jwt;
    SharedPreferences pref;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    //paging
    @BindView(R.id.pager)
    ViewPager2 viewPager2;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.frag_home_layout)
    LinearLayout linearLayout;

    UserViewModel userViewModel;
    String address;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpBar(savedInstanceState);
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)
                .setDrawerLayout(drawer)
                .build();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (clickedItem != 0){
                    handleClick();
                }
            }
        };
        drawer.addDrawerListener(toggle);

        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        View view = navigationView.getHeaderView(0);
        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvEmail = view.findViewById(R.id.tv_email);
        check(tvName, tvEmail);
        //fetchAppCategories();
    }



    private void check(TextView tvName, TextView tvEmail) {

        //LiveData<User> user = userViewModel.getUser();
        userViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user != null){
                    tvEmail.setText(user.getEmail());
                    tvName.setText(user.getFirstName()+ " "+user.getLastName());
                }else{
                    //the user might have a valid jwt use it to login
                    Log.e(TAG, "the user might have a valid jwt use it to login");
                    if (checkJwt()){
                        profile();
                    }

                }

            }
        });
    }

    private void profile() {
        pref = getApplication().getSharedPreferences(Constants.getPrefJwt(),MODE_PRIVATE);
        jwt = pref.getString("jwt", null);
        NetworkData getData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiUserResponse> getProf = getData.getProfile(jwt);
        getProf.enqueue(new Callback<ApiUserResponse>() {
            @Override
            public void onResponse(Call<ApiUserResponse> call, Response<ApiUserResponse> response) {
                if (response.body() != null) {
                    ApiUserResponse apiUserResponse = response.body();
                    boolean msg = Boolean.parseBoolean(apiUserResponse.getMessage());
                    if (msg) {

                        new UserViewModel(MainActivity.this.getApplication()).saveUserData(apiUserResponse);


                    }
                }
            }

            @Override
            public void onFailure(Call<ApiUserResponse> call, Throwable t) {

            }
        });

    }


    private Boolean checkJwt(){
        pref = getApplication().getSharedPreferences(Constants.getPrefJwt(),MODE_PRIVATE);
        jwt = pref.getString("jwt", null);
        if (jwt == null){
            return  false;
        }else{
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    private String getHumanAddress(String address){

        Log.e(TAG, address);
        return this.address;
    }
    private void setUpBar(Bundle savedInstanceState) {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            lat = getIntent().getDoubleExtra(MainActivity.LAT,0);
            lng = getIntent().getDoubleExtra(MainActivity.LNG, 0);
            fetchAppCategories(lat, lng);
            //getGeocodedPlace(lat, lng);

        }

    }
    private void noStores(){
        //use toolbar
        Snackbar.make(toolbar, R.string.no_stores, BaseTransientBottomBar.LENGTH_SHORT).show();
    }


    private void handleClick(){
        switch (clickedItem){
            case R.id.nav_orders:
                startActivity(new Intent(getApplicationContext(), OrderActivity.class));
                break;
            case R.id.nav_chat:
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
                //ChatFragment chatFragment = new ChatFragment();
                //getSupportFragmentManager().beginTransaction().add(chatFragment,"crisp_fragment").commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        clickedItem = item.getItemId();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void fetchAppCategories(Double lat, Double lng){
        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiAppCategoriesResponse> appCategoriesCall = networkData.getAppCategories(lat, lng);
        appCategoriesCall.enqueue(new Callback<ApiAppCategoriesResponse>() {
            @Override
            public void onResponse(Call<ApiAppCategoriesResponse> call, Response<ApiAppCategoriesResponse> response) {
                if (response.body() != null){
                    String msg = response.body().getMessage();
                    if (Boolean.parseBoolean(msg)){
                        moo(response.body().getAppCategory().getAppCategories());
                        getGeocodedPlace(lat,lng);
                    }else{
                        noStores();
                    }


                }
            }

            @Override
            public void onFailure(Call<ApiAppCategoriesResponse> call, Throwable t) {

            }
        });
    }

    private void moo(ArrayList<AppCategory> appCategories) {
        Log.e(TAG, "size is " + appCategories.size());
            storeCollectionAdapter = new StoreCollectionAdapter(this, appCategories);
            viewPager2.setAdapter(storeCollectionAdapter);

            new TabLayoutMediator(tabLayout, viewPager2,
                    (tab, position) -> {
                        tab.setText( appCategories.get(position).getName());

                    }
            ).attach();


    }

    private void getGeocodedPlace(double latitude, double longitude) {
        String lat = String.valueOf(latitude);
        String lng = String.valueOf(longitude);
        String latlng = lat + "," +lng;
        Log.e(TAG, latlng);
        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<GetGeocodedPlace> call = networkData.mGetGeocodedPlace(
                latlng,
                getResources().getString(R.string.result_type),
                getResources().getString(R.string.google_maps_api_key));
        call.enqueue(new Callback<GetGeocodedPlace>() {
            @Override
            public void onResponse(Call<GetGeocodedPlace> call, Response<GetGeocodedPlace> response) {
                if (response.body() != null){
                    GetGeocodedPlace cc = response.body();
                    String foo = cc.getStatus();

                    if (foo.equals("OK")){
                        //well geocoded
                        Log.e(TAG, "well geocoded");
                        //getHumanAddress();
                        String address = cc.getResults().get(0).getAddress();
                        if (address != null && getSupportActionBar()!=null){
                            getSupportActionBar().setTitle(address);
                        }else{
                            getSupportActionBar().setTitle("Current location");
                        }
                        //start main activity


                    }else{
                        if (getSupportActionBar()!=null){
                            getSupportActionBar().setTitle("Current location");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetGeocodedPlace> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });

    }


}
