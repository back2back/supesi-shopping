package co.supesi.shopping.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.BasicApp;
import co.supesi.shopping.MainActivity;
import co.supesi.shopping.R;
import co.supesi.shopping.database.DataRepository;
import co.supesi.shopping.model.FindStoreDelivery;
import co.supesi.shopping.model.GetGeocodedPlace;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;
import co.supesi.shopping.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    Unbinder unbinder;

    private final String TAG = SplashActivity.class.getSimpleName();
    public static final int ERROR_DIALOG_REQUEST = 9001;
    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9002;
    public static final int PERMISSIONS_REQUEST_ENABLE_GPS = 9003;

    double latitude;
    double longitude;

    SharedPreferences pref;

    FusedLocationProviderClient fusedLocationProviderClient;
    private boolean mLocationPermissionGranted = false;
    private String jwt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        unbinder = ButterKnife.bind(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (checkMapServices()){
            if (mLocationPermissionGranted){
                getLatestLocation();
            }else{
                getLocationPermission();
            }
        }
    }

    private boolean checkMapServices(){
        if(isServicesOK()){
            if(isMapsEnabled()){
                return true;
            }
        }
        return false;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This application requires GPS to work properly, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean isMapsEnabled(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            getLatestLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(SplashActivity.this);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(SplashActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ENABLE_GPS: {
                if(mLocationPermissionGranted){
                    getLatestLocation();
                }
                else{
                    getLocationPermission();
                }
            }
        }

    }

    private void getLatestLocation() {
        fusedLocationProviderClient = new FusedLocationProviderClient(this);
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    insertLocation(latitude, longitude);
                    //isUserSignedIn();

                    //getGeocodedPlace(latitude, longitude);

                }
            }
        });

    }

    private void insertLocation(double latitude, double longitude){
        String lat = String.valueOf(latitude);
        String lng = String.valueOf(longitude);
        FindStoreDelivery findStoreDelivery = new FindStoreDelivery();
        findStoreDelivery.setLat(lat);
        findStoreDelivery.setLng(lng);
        Log.e(TAG, lat);
        Log.e(TAG, lng);
        findStoreDelivery.setDevice_id(Constants.getDeviceId());
        DataRepository dataRepository = new DataRepository(BasicApp.getContext());
        dataRepository.insertLocation(findStoreDelivery);

        isUserSignedIn();
    }

    private void isUserSignedIn() {
        if (checkJwt()){
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            intent.putExtra(MainActivity.LAT, latitude);
            intent.putExtra(MainActivity.LNG, longitude);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(SplashActivity.this, LoginSigninActivity.class);
            startActivity(intent);
            finish();
        }

    }
    private Boolean checkJwt(){
        pref = getApplication().getSharedPreferences(Constants.getPrefJwt(),MODE_PRIVATE);
        jwt = pref.getString("jwt", null);
        if (jwt == null){
            return  false;
        }else{
            return true;
        }
    }

    private void getGeocodedPlace(double latitude, double longitude) {
        String lat = String.valueOf(latitude);
        String lng = String.valueOf(longitude);
        String latlng = lat + "," +lng;
        Log.e(TAG, latlng);
        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<GetGeocodedPlace> call = networkData.mGetGeocodedPlace(
                latlng,
                getResources().getString(R.string.result_type),
                getResources().getString(R.string.google_maps_api_key));
        call.enqueue(new Callback<GetGeocodedPlace>() {
            @Override
            public void onResponse(Call<GetGeocodedPlace> call, Response<GetGeocodedPlace> response) {
                if (response.body() != null){
                    GetGeocodedPlace cc = response.body();
                    /*String foo = cc.getStatus();
                    if (foo == "OK"){
                        //well geocoded
                        Log.e(TAG, "well geocoded");

                        //start main activity


                    }*/
                }
            }

            @Override
            public void onFailure(Call<GetGeocodedPlace> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });

    }


}
