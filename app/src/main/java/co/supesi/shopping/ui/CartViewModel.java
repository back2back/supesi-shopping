package co.supesi.shopping.ui;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import co.supesi.shopping.database.DataRepository;
import co.supesi.shopping.model.Order;

public class CartViewModel extends AndroidViewModel {

    private static final String TAG = CartViewModel.class.getSimpleName();
    private DataRepository dataRepository;

    private LiveData<List<Order>> orders;
    private LiveData<Integer> sumTotal;
    public CartViewModel(@NonNull Application application) {
        super(application);

        dataRepository = new DataRepository(application);
        orders = dataRepository.getAllOrders();
        sumTotal = dataRepository.getTotals();

    }



    public LiveData<Integer> getSumTotal(){
        return sumTotal;
    }
    public LiveData<List<Order>> getOrders(){
        return orders;
    }

    public void updateQuantity(Order order){
        dataRepository.updateOrder(order);
    }
    public void insertOrder(Order order){
        dataRepository.insertOrder(order);
    }
    public void deleteOrders(){
        dataRepository.deleteAllOrders();
    }
}
