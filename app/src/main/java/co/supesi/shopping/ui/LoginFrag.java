package co.supesi.shopping.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.R;
import co.supesi.shopping.model.ApiUserResponse;
import co.supesi.shopping.model.User;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;
import co.supesi.shopping.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFrag extends Fragment {
    private Unbinder unbinder;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @BindView(R.id.tv_signup_instead)
    TextView signupInstead;
    @BindView(R.id.email_lyt)
    TextInputLayout emailLyt;
    @BindView(R.id.password_lyt)
    TextInputLayout passwordLyt;
    @BindView(R.id.btn_login)
    Button btnLogin;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LoginFrag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFrag.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFrag newInstance(String param1, String param2) {
        LoginFrag fragment = new LoginFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        signupInstead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swapFrag();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin(v);
            }
        });
    }

    private void attemptLogin(View v) {
        String email = emailLyt.getEditText().getText().toString();
        String password = passwordLyt.getEditText().getText().toString();

        ArrayList<String> data = new ArrayList<>();
        data.add(email);
        data.add(password);

        for (String foo: data){
            if(!isNotEmpty(foo)){
                Toast.makeText(this.getContext(), "Check all fields ", Toast.LENGTH_SHORT).show();
                return;
            };
        }
        hideKeyboard();
        login(email, password, v);
    }

    private void login(String email, String password, View v) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        NetworkData getData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiUserResponse> loginUser = getData.login(user);
        loginUser.enqueue(new Callback<ApiUserResponse>() {
            @Override
            public void onResponse(Call<ApiUserResponse> call, Response<ApiUserResponse> response) {
                if (response.body() != null){
                    ApiUserResponse apiUserResponse = response.body();
                    boolean msg = Boolean.parseBoolean(apiUserResponse.getMessage());
                    if (msg){
                        writePref(apiUserResponse.getJwt());
                        new UserViewModel(getActivity().getApplication()).saveUserData(apiUserResponse);
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                    }else {
                        Snackbar.make(v,"Account could not be created", BaseTransientBottomBar.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiUserResponse> call, Throwable t) {

            }
        });
    }

    private void writePref(String jwt){
        pref = requireActivity().getApplication().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString("jwt", jwt);
        editor.commit();
    }
    public boolean isNotEmpty(String text){

        return text.length()>0;
    }

    private void hideKeyboard() {
        View view = requireActivity().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
    private void swapFrag() {
        SignUpFrag signUpFrag = new SignUpFrag();
        FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_container, signUpFrag);

        transaction.commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
