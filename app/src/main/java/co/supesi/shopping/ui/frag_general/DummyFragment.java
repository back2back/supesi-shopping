package co.supesi.shopping.ui.frag_general;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.MainActivity;
import co.supesi.shopping.R;
import co.supesi.shopping.adapter.StoreAdapter;
import co.supesi.shopping.model.ApiStoresResponse;
import co.supesi.shopping.model.AppCategory;
import co.supesi.shopping.model.Store;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DummyFragment extends Fragment {
    public static final String ARG_OBJECT = "object";
    private String TAG = DummyFragment.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.rv_stores)
    RecyclerView rvStores;
    @BindView(R.id.dummy_fragment_layout)
    ConstraintLayout constraintLayout;

    AppCategory appCategory;
    MainActivity mainActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dummy, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        Bundle args = getArguments();
        appCategory = (AppCategory) args.getSerializable(DummyFragment.ARG_OBJECT);

        foobar(appCategory.getStores());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void foobar(ArrayList<Store> stores){

        StoreAdapter storeAdapter = new StoreAdapter(getContext(), stores);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvStores.setLayoutManager(layoutManager);
        rvStores.setAdapter(storeAdapter);
    }

}
