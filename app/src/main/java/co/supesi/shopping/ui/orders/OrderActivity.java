package co.supesi.shopping.ui.orders;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.R;
import co.supesi.shopping.adapter.OrdersPagerAdapter;

public class OrderActivity extends AppCompatActivity {

    Unbinder unbinder;
    OrdersPagerAdapter ordersPagerAdapter;
    @BindView(R.id.order_layout)
    LinearLayout layout;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager2 viewPager2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        setUp();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpViewPager();

    }

    private void setUp() {
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");

        progressDialog.show();

    }

    private void setUpViewPager(){

        ordersPagerAdapter = new OrdersPagerAdapter(this);
        viewPager2.setAdapter(ordersPagerAdapter);

        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                if (position == 0){
                    tab.setText(getResources().getText(R.string.current_order));
                }else{
                    tab.setText(getResources().getText(R.string.past_orders));
                }

            }
        }).attach();

        progressDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
