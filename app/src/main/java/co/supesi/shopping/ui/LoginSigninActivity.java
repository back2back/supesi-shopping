package co.supesi.shopping.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.R;

public class LoginSigninActivity extends AppCompatActivity {


    private static final String TAG = "LoginSigninActivity";
    Unbinder unbinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signin);
        setUp(savedInstanceState);

    }

    private void setUp(Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this);
        if (savedInstanceState != null){
            return;
        }
        SignUpFrag signUpFrag = new SignUpFrag();
        getSupportFragmentManager().beginTransaction().add(R.id.frag_container,signUpFrag).commit();
    }


}
