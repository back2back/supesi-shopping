package co.supesi.shopping.ui.orders;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.R;
import co.supesi.shopping.model.ApiCurrentOrder;
import co.supesi.shopping.model.OrderEntry;
import co.supesi.shopping.model.RiderLocation;
import co.supesi.shopping.model.Store;
import co.supesi.shopping.model.Trip;
import co.supesi.shopping.model.User;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;
import co.supesi.shopping.utility.Constants;
import co.supesi.shopping.utility.LatLngInterpolator;
import co.supesi.shopping.utility.MarkerAnimation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CurrentOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CurrentOrderFragment extends Fragment implements OnMapReadyCallback {

    Unbinder unbinder;
    @BindView(R.id.map_view)
    MapView mapView;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.btn_call_rider)
    Button btnCallRider;
    @BindView(R.id.tv_amount)
    TextView tvAmount;


    GoogleMap googleMap;
    private SharedPreferences pref;
    private String jwt;
    ArrayList<Marker> markerArrayList;

    private static final String TAG = "CurrentOrderFragment";

    public CurrentOrderFragment() {
        // Required empty public constructor
    }


    public static CurrentOrderFragment newInstance() {
        CurrentOrderFragment fragment = new CurrentOrderFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_current_order, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = requireActivity().getSharedPreferences(Constants.getPrefJwt(),MODE_PRIVATE);
        jwt = pref.getString("jwt", null);
        Log.e(TAG, "on view created");

        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
        currentOrder();

    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        googleMap = null;
        Log.e(TAG, "on destroy called");

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap=googleMap;
        this.googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(), R.raw.map_style));
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        Log.e(TAG, "on pause called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView!= null){
            mapView.onDestroy();
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView!= null){
            mapView.onDestroy();
        }
    }

    public void currentOrder(){
        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiCurrentOrder> call = networkData.getCurrentOrder(jwt);
        call.enqueue(new Callback<ApiCurrentOrder>() {
            @Override
            public void onResponse(Call<ApiCurrentOrder> call, Response<ApiCurrentOrder> response) {
                if (response.body() != null){
                    String msg = response.body().getMessage();
                    Log.e(TAG, msg);
                    if (Boolean.parseBoolean(msg)){
                        parseData(response.body());
                    }else{
                        noTrip();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiCurrentOrder> call, Throwable t) {

            }
        });
    }

    private void noTrip() {
        tvStatus.setText("No trip available");
    }

    private void parseData(ApiCurrentOrder body) {

        OrderEntry orderEntry = new OrderEntry();
        orderEntry = body.getOrderEntry();
        int status = orderEntry.getStatus();
        Store store = orderEntry.getStore();


        switch (status){
            case 1:


                showStoreMarker(store.getLat(), store.getLng());
                tvStatus.setText("Getting ready");
                break;
            case 3:


                showStoreMarker(store.getLat(), store.getLng());
                tvStatus.setText("Getting rider");
                break;
            case 4:
                showRiderMarker(orderEntry, body.getTrip());
                break;
            default:
                tvStatus.setText("No trip available");
        }

    }

    private void showRiderMarker(OrderEntry orderEntry, Trip trip) {
        //find which user is doing this trip

        riderDetails(orderEntry, trip);
        //readFirestore();
        updates(trip);
    }

    private void riderDetails(OrderEntry orderEntry, Trip trip) {
        User user = trip.getUser();
        String phone = user.getPhone();
        btnCallRider.setVisibility(View.VISIBLE);
        btnCallRider.setText("Call " + user.getFirstName());
        btnCallRider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+phone));
                v.getContext().startActivity(intent);
            }
        });
        tvStatus.setText("En route");
        int total = trip.getNiceAmount() + orderEntry.getTotal();
        tvAmount.setVisibility(View.VISIBLE);
        tvAmount.setText("KES " + total);
    }

    private void updates(Trip trip){
        User user = trip.getUser();
        String f = String.valueOf(user.getId());
        markerArrayList = new ArrayList<>();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        final DocumentReference docRef = db.collection(getString(R.string.location)).document(f);
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (e != null){
                    Log.e(TAG, "Listen failed.", e);
                    return;
                }

                if (documentSnapshot != null && documentSnapshot.exists()){
                    //Log.e(TAG, "data bitch " + documentSnapshot.getData());
                    RiderLocation riderLocation = documentSnapshot.toObject(RiderLocation.class);
                    double lat = riderLocation.getGeoPoint().getLatitude();
                    double lng = riderLocation.getGeoPoint().getLongitude();

                    mapRider(lat, lng);

                }else{
                    Log.e(TAG, "current data is null");
                }
            }
        });
    }

    private void mapRider(double lat, double lng) {
        Log.e(TAG, "map rider called");
        if (googleMap != null){


            LatLng riderLocation = new LatLng(lat,lng);

            MarkerOptions markerOption = new MarkerOptions();
            markerOption.position(riderLocation).title("foo");

           Marker marker = googleMap.addMarker(markerOption);

           markerArrayList.add(marker);


            googleMap.moveCamera(CameraUpdateFactory.newLatLng(riderLocation));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));


            int size = markerArrayList.size();


            for (int x=0; x < markerArrayList.size(); x++){

                if (x > 0){
                     markerArrayList.get(x-1).remove();

                }
               /* Marker curr = markerArrayList.get(x);
                MarkerAnimation.animateMarkerToICS(old,
                        curr.getPosition(),
                        new LatLngInterpolator.Linear());*/


            }


        }





    }


    private void showStoreMarker(String lat, String lng) {
        LatLng storeLocation = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
        googleMap.addMarker(new MarkerOptions()
                .position(storeLocation)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.home))
                .title("At shop"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(storeLocation));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

}
