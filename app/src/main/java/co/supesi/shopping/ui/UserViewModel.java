package co.supesi.shopping.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import co.supesi.shopping.database.DataRepository;
import co.supesi.shopping.model.ApiUserResponse;
import co.supesi.shopping.model.User;


public class UserViewModel extends AndroidViewModel {
    private DataRepository dataRepository;
    private LiveData<User> user;

    public UserViewModel(@NonNull Application application) {
        super(application);
        dataRepository =new DataRepository(application);
        user = dataRepository.getUser();
    }

    public LiveData<User> getUser(){
        return user;
    }
    public void saveUserData(ApiUserResponse apiUserResponse) {
        User user = apiUserResponse.getSingleUser();
        dataRepository.insertUser(user);
    }
}
