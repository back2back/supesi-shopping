package co.supesi.shopping.ui.orders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.PrimaryKey;

import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.R;
import co.supesi.shopping.adapter.PastOrdersAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class PastOrdersFragment extends Fragment {

    private static final String TAG = "PastOrdersFragment";
    private ArrayList<String> data;
    PastOrdersAdapter pastOrdersAdapter;
    Unbinder unbinder;

    @BindView(R.id.rv_past_orders)
    RecyclerView rvPastOrders;
    @BindView(R.id.tv_nothing)
    TextView nothing;


    public PastOrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_past_orders, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }

    private void setAdapter() {
        data = new ArrayList<>();
        /*data.add("One");
        data.add("two");
        data.add("faf");
        data.add("fasd");
        data.add("Odafdfne");*/

        if (data.size() > 0){
            pastOrdersAdapter = new PastOrdersAdapter(getContext(), data);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            rvPastOrders.setLayoutManager(layoutManager);
            rvPastOrders.setAdapter(pastOrdersAdapter);
        }else{
            nothing.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
