package co.supesi.shopping.ui.store;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.L;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.BasicApp;
import co.supesi.shopping.CheckoutActivity;
import co.supesi.shopping.R;
import co.supesi.shopping.adapter.ProductsAdapter;
import co.supesi.shopping.adapter.StoreAdapter;
import co.supesi.shopping.database.DataRepository;
import co.supesi.shopping.model.ApiStoreProductsResponse;
import co.supesi.shopping.model.FindStoreDelivery;
import co.supesi.shopping.model.Order;
import co.supesi.shopping.model.StoreProduct;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;
import co.supesi.shopping.ui.CartViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreActivity extends AppCompatActivity {

    private final String TAG = StoreActivity.class.getSimpleName();
    private CartViewModel cartViewModel;
    Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_image)
    ImageView ivImage;
    @BindView(R.id.rv_products)
    RecyclerView rvCategories;
    @BindView(R.id.btn_checkout)
    Button btnCheckout;
    @BindView(R.id.tv_store_description)
    TextView tvStoreDescription;
    @BindView(R.id.tv_del_fee)
            TextView delFee;
    ProgressDialog progressDialog;

    int storeId;
    String description;
    String lat;
    String lng;
    String delivery;
    AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        setUp();
        cartViewModel = new ViewModelProvider(this).get(CartViewModel.class);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading products");
        progressDialog.show();



    }

    @Override
    protected void onResume() {
        super.onResume();
        cartViewModel.getOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(List<Order> orders) {

                if (orders.size() > 0){
                    btnCheckout.setVisibility(View.VISIBLE);
                }else {
                    btnCheckout.setVisibility(View.GONE);
                }



            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

       if (alert != null){
           alert.dismiss();
       }

    }

    private void setUp(){
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");

        }

        description = getIntent().getStringExtra(StoreAdapter.STORE_DESCRIPTION);
        if (description !=null){
            tvStoreDescription.setText(description);
        }else{
            tvStoreDescription.setText(R.string.sample_text);
        }
        String image = getIntent().getStringExtra(StoreAdapter.STORE_BANNER);
        Glide.with(this).load(image).into(ivImage);


        fetchItems();


        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StoreActivity.this, CheckoutActivity.class);
                intent.putExtra(CheckoutActivity.DELIVERY, delivery);
                intent.putExtra(CheckoutActivity.LAT, lat);
                intent.putExtra(CheckoutActivity.LNG, lng);
                intent.putExtra(CheckoutActivity.STORE_ID,String.valueOf(storeId));
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (cartViewModel.getOrders().getValue().size() > 0) {
            alertClearCart();
        }else {
            super.onBackPressed();
        }
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void foobar(String lat, String lng){
        this.lat =lat;
        this.lng = lng;
        storeId = getIntent().getIntExtra(StoreAdapter.STORE_EXTRA, 0);
        FindStoreDelivery findStoreDelivery = new FindStoreDelivery();
        findStoreDelivery.setStoreId(String.valueOf(storeId));
        findStoreDelivery.setLat(lat);
        findStoreDelivery.setLng(lng);
        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);

        Call<ApiStoreProductsResponse> responseCall = networkData.getProducts(findStoreDelivery);
        responseCall.enqueue(new Callback<ApiStoreProductsResponse>() {
            @Override
            public void onResponse(Call<ApiStoreProductsResponse> call, Response<ApiStoreProductsResponse> response) {
                if (response.body() != null){

                    parseResponse(response.body());
                }
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<ApiStoreProductsResponse> call, Throwable t) {
                Log.e(TAG, "failed " + t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    private void fetchItems(){


        DataRepository dataRepository = new DataRepository(BasicApp.getContext());
        LiveData<FindStoreDelivery> findStoreDeliveryLiveData =
                dataRepository.getLatLng();


        findStoreDeliveryLiveData.observe(this, new Observer<FindStoreDelivery>() {
            @Override
            public void onChanged(FindStoreDelivery findStoreDelivery) {
                 lat = findStoreDelivery.getLat();
                 lng = findStoreDelivery.getLng();

                 foobar(lat,lng);

            }
        });



    }

    private void parseResponse(ApiStoreProductsResponse body) {
       String message = body.getMessage();
       if (Boolean.parseBoolean(message)){
           //set up adapters and shit
           setAdapter(body.getStoreProducts());
           delivery = body.getDelivery();
           delFee.setText (getString(R.string.kes, delivery));
       }else{
           Toast.makeText(this, "Could not load store products", Toast.LENGTH_SHORT).show();
       }
    }

    private void setAdapter(ArrayList<StoreProduct> storeProducts) {
        rvCategories.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvCategories.setAdapter(new ProductsAdapter(this, cartViewModel, storeProducts));
    }

    private void alertClearCart() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Your cart will be cleared. Are you sure?")
                .setCancelable(false)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        deleteOrders();
                        StoreActivity.super.onBackPressed();
                    }
                });
         alert = builder.create();
         alert.show();


    }

    private void deleteOrders() {
        cartViewModel.deleteOrders();
    }
}
