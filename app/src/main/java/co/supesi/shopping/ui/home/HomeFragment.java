package co.supesi.shopping.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.R;
import co.supesi.shopping.adapter.StoreCollectionAdapter;
import co.supesi.shopping.model.AppCategoriesResponse;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    Unbinder unbinder;
    private HomeViewModel homeViewModel;

    @BindView(R.id.pager)
    ViewPager2 viewPager2;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.frag_home_layout)
    LinearLayout linearLayout;


    StoreCollectionAdapter storeCollectionAdapter;
    

    private String TAG = "HomeFragment";


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View inflate = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        //storeCollectionAdapter = null;
        Log.e(TAG, "On destroy called");
    }








}
