package co.supesi.shopping.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.shopping.R;
import co.supesi.shopping.model.Order;
import co.supesi.shopping.ui.CartViewModel;

public class CheckoutItemsAdapter extends RecyclerView.Adapter<CheckoutItemsAdapter.CheckoutItemsViewHolder> {

    ArrayList<Order> orders;
    Context context;
    LayoutInflater layoutInflater;
    CartViewModel cartViewModel;
    public CheckoutItemsAdapter(Context context, CartViewModel cartViewModel){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.cartViewModel = cartViewModel;

    }
    @NonNull
    @Override
    public CheckoutItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.checkout_item, parent, false);
        return new CheckoutItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckoutItemsViewHolder holder, int position) {
        Order order = orders.get(position);
        holder.tvProdName.setText(order.getProductName());
        holder.tvQty.setText(String.valueOf(order.getQuantity()));
        int foobar = order.getQuantity() * order.getPrice();
        holder.tvAmount.setText(String.valueOf(foobar));

        //add and remove
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = order.getQuantity();
                qty++;
                order.setQuantity(qty);

                cartViewModel.updateQuantity(order);
            }
        });

        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = order.getQuantity();
                qty--;
                order.setQuantity(qty);
                cartViewModel.updateQuantity(order);
            }
        });


    }

    public void setOrders(List<Order> orders){
        this.orders = (ArrayList<Order>) orders;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        if (orders != null){
            return orders.size();
        }else return 0;
    }

    class CheckoutItemsViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_prod_name)
        TextView tvProdName;
        @BindView(R.id.tv_qty)
        TextView tvQty;
        @BindView(R.id.tv_amount)
        TextView tvAmount;
        @BindView(R.id.iv_remove)
        ImageView ivRemove;
        @BindView(R.id.iv_add)
        ImageView ivAdd;

        public CheckoutItemsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
