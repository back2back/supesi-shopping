package co.supesi.shopping.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;

import co.supesi.shopping.model.AppCategory;
import co.supesi.shopping.ui.frag_general.DummyFragment;

public class StoreCollectionAdapter extends FragmentStateAdapter {

    //this sets up tabs
    private ArrayList<AppCategory> responses;
    public StoreCollectionAdapter(FragmentActivity fragment, ArrayList<AppCategory> responses) {
        super(fragment);
        this.responses = responses;

    }



    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = new DummyFragment();
        Bundle args = new Bundle();

        args.putSerializable(DummyFragment.ARG_OBJECT, responses.get(position));
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public int getItemCount() {
        return responses.size();
    }
}
