package co.supesi.shopping.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.shopping.R;

public class PastOrdersAdapter extends RecyclerView.Adapter<PastOrdersAdapter.PastOrdersViewHolder> {

    LayoutInflater layoutInflater;
    ArrayList<String> fooz;
    private static final String TAG = "PastOrdersAdapter";
    public PastOrdersAdapter(Context context, ArrayList<String> fooz){
        layoutInflater= LayoutInflater.from(context);
        this.fooz = fooz;
    }
    @NonNull
    @Override
    public PastOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.past_orders,parent, false);

        return new PastOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PastOrdersViewHolder holder, int position) {
        holder.tvOrderTitle.setText(fooz.get(position));
    }

    @Override
    public int getItemCount() {
        return fooz.size();
    }

    class PastOrdersViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_order_title)
        TextView tvOrderTitle;
        public PastOrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
