package co.supesi.shopping.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import co.supesi.shopping.ui.orders.CurrentOrderFragment;
import co.supesi.shopping.ui.orders.PastOrdersFragment;

public class OrdersPagerAdapter extends FragmentStateAdapter {

    private final int MAX_FRAGS = 2;

    public OrdersPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);

    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment;
        Bundle args;

        switch (position){
            case 0:
                fragment = new CurrentOrderFragment();

                return fragment;
            case 1:
                fragment = new PastOrdersFragment();
                return fragment;

            default:
                throw new IllegalStateException("Unexpected value: " + position);
        }

    }


    @Override
    public int getItemCount() {
        return MAX_FRAGS;
    }
}
