package co.supesi.shopping.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.shopping.R;
import co.supesi.shopping.model.Store;
import co.supesi.shopping.ui.store.StoreActivity;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.StoreViewHolder> {

    LayoutInflater layoutInflater;
    ArrayList<Store> stores;

    private final String TAG = StoreAdapter.class.getSimpleName();
    public static final String STORE_EXTRA = "foo";
    public static final String STORE_DESCRIPTION = "BAR";
    public static final String STORE_BANNER = "BANNER";

    public StoreAdapter(Context context, ArrayList<Store> stores){
        layoutInflater = LayoutInflater.from(context);
        this.stores = stores;
    }
    @NonNull
    @Override
    public StoreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.stores, parent, false);

        return new StoreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreViewHolder holder, int position) {
        if (stores != null){
            Store store = stores.get(position);
            String imagePath = store.getBanner();

            holder.storeName.setText(store.getName());
            //cardview
            holder.cvStore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), StoreActivity.class);
                    intent.putExtra(STORE_EXTRA, store.getId());
                    intent.putExtra(STORE_DESCRIPTION, store.getDescription());
                    intent.putExtra(STORE_BANNER, imagePath);
                    v.getContext().startActivity(intent);
                }
            });

            Glide.with(holder.itemView).load(imagePath)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        if (stores != null){
            return stores.size();
        }
        return 0;
    }


    class StoreViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_store_name)
        TextView storeName;
        @BindView(R.id.card_view_stores)
        CardView cvStore;
        @BindView(R.id.iv_store_banner)
        ImageView imageView;

        public StoreViewHolder(@NonNull View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
