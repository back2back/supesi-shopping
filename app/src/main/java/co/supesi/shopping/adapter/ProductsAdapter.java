package co.supesi.shopping.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.shopping.BasicApp;
import co.supesi.shopping.R;
import co.supesi.shopping.model.Order;
import co.supesi.shopping.model.Product;
import co.supesi.shopping.model.StoreProduct;
import co.supesi.shopping.ui.CartViewModel;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder>{

    private Context context;
    CartViewModel cartViewModel;


    private ArrayList<StoreProduct> storeProducts;

    public ProductsAdapter(Context context, CartViewModel cartViewModel, ArrayList<StoreProduct> storeProducts) {
        this.context = context;
        this.cartViewModel = cartViewModel;
        this.storeProducts = storeProducts;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_image_banner)
        ImageView ivPorductImage;
        @BindView(R.id.tv_product_name)
        TextView tvProductName;
        @BindView(R.id.tv_description)
        TextView description;
        @BindView(R.id.tv_price)
        TextView price;
        @BindView(R.id.btn_add_cart)
        Button addCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        StoreProduct storeProduct = storeProducts.get(position);
        Product product = storeProduct.getProduct();

        Glide.with(context).load(product.getBanner()).into(holder.ivPorductImage);
        holder.tvProductName.setText(product.getTitle());
        holder.description.setText(product.getDescription());
        holder.price.setText("KES " + product.getSellingPrice());
        holder.addCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //insert into db
                Order order = new Order();
                order.setProduct_id(product.getId());
                order.setProductName(product.getTitle());
                order.setQuantity(1);
                order.setPrice(product.getSellingPrice());

                cartViewModel.insertOrder(order);
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.view_product_item, parent, false));
    }

    @Override
    public int getItemCount() {
        return storeProducts.size();
    }
}
