package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiAppCategoriesResponse {
    @SerializedName("message")
    private String message;


    @SerializedName("appCategories")
    private AppCategory appCategory;

    public AppCategory getAppCategory() {
        return appCategory;
    }

    public void setAppCategory(AppCategory appCategory) {
        this.appCategory = appCategory;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }




}
