package co.supesi.shopping.model;

import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class RiderLocation {
    private GeoPoint geoPoint;
    @SerializedName("user")
    private User user;

    private String user_id;
    private @ServerTimestamp
    Date timestamp;

    private double lat;
    private double lng;

    public RiderLocation() {

    }
    public RiderLocation(GeoPoint geoPoint, User user) {
        this.geoPoint = geoPoint;
        this.user = user;
        this.timestamp = timestamp;
        this.lat = geoPoint.getLatitude();
        this.lng = geoPoint.getLongitude();
    }

    public RiderLocation(GeoPoint geoPoint, String user_id) {
        this.geoPoint = geoPoint;
        this.user_id = user_id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "RiderLocation{" +
                "geoPoint=" + geoPoint +
                ", user=" + user +
                ", user_id='" + user_id + '\'' +
                ", timestamp=" + timestamp +
                ", lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
