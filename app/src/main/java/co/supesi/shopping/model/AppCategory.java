package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AppCategory implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("stores")
    private ArrayList<Store> stores;
    @SerializedName("data")
    private ArrayList<AppCategory> appCategories;

    public ArrayList<AppCategory> getAppCategories() {
        return appCategories;
    }

    public void setAppCategories(ArrayList<AppCategory> appCategories) {
        this.appCategories = appCategories;
    }

    public ArrayList<Store> getStores() {
        return stores;
    }

    public void setStores(ArrayList<Store> stores) {
        this.stores = stores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AppCategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", stores=" + stores +
                ", appCategories=" + appCategories +
                '}';
    }
}
