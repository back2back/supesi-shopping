package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

public class ApiCurrentOrder {
    @SerializedName("message")
    private String message;
    @SerializedName("orderEntry")
    private OrderEntry orderEntry;
    @SerializedName("trip")
    private Trip trip;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OrderEntry getOrderEntry() {
        return orderEntry;
    }

    public void setOrderEntry(OrderEntry orderEntry) {
        this.orderEntry = orderEntry;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }
}
