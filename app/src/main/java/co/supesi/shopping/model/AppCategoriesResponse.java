package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AppCategoriesResponse {
    @SerializedName("message")
    private String message;

    @SerializedName("name")
    private String name;

    @SerializedName("stores")
    private ArrayList<Store> stores;

    @SerializedName("appCategories")
    private ArrayList<AppCategoriesResponse> appCategoryResponses;

    public ArrayList<AppCategoriesResponse> getAppCategoryResponses() {
        return appCategoryResponses;
    }

    public void setAppCategoryResponses(ArrayList<AppCategoriesResponse> appCategoryResponses) {
        this.appCategoryResponses = appCategoryResponses;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Store> getStores() {
        return stores;
    }

    public void setStores(ArrayList<Store> stores) {
        this.stores = stores;
    }
}
