package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Store implements Serializable {

    @SerializedName("name")
    private String name;
    @SerializedName("banner")
    private String banner;
    @SerializedName("id")
    private int id;
    @SerializedName("description")
    private String description;
    @SerializedName("app-category")
    private AppCategory appCategory;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;

    }

    public AppCategory getAppCategory() {
        return appCategory;
    }

    public void setAppCategory(AppCategory appCategory) {
        this.appCategory = appCategory;
    }

    @Override
    public String toString() {
        return "Store{" +
                "name='" + name + '\'' +
                ", banner='" + banner + '\'' +
                ", id=" + id +
                ", description='" + description + '\'' +
                ", appCategory=" + appCategory +
                '}';
    }
}
