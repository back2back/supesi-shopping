package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class GetGeocodedPlace {
    @SerializedName("status")
    private String status;
    @SerializedName("results")
    private List<GoogleResults> results;

    public List<GoogleResults> getResults() {
        return results;
    }

    public void setResults(List<GoogleResults> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "GetGeocodedPlace{" +
                "status='" + status + '\'' +
                ", results=" + results +
                '}';
    }
}
