package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiStoresResponse {

    @SerializedName("message")
    private String ok;

    @SerializedName("stores")
    private ArrayList<Store> stores;



    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public ArrayList<Store> getStores() {
        return stores;
    }

    public void setStores(ArrayList<Store> stores) {
        this.stores = stores;
    }
}
