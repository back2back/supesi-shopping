package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

public class ApiOrderPlacedResponse {
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
