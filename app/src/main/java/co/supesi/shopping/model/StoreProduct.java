package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

public class StoreProduct {
    @SerializedName("id")
    private int id;
    @SerializedName("product")
    private Product product;
    @SerializedName("store")
    private Store store;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
