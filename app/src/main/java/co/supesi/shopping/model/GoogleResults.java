package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

public class GoogleResults {
    @SerializedName("formatted_address")
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
