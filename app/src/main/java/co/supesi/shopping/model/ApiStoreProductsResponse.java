package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiStoreProductsResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("storeProducts")
    private ArrayList<StoreProduct> storeProducts;

    @SerializedName("delivery")
    private String delivery;

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<StoreProduct> getStoreProducts() {
        return storeProducts;
    }

    public void setStoreProducts(ArrayList<StoreProduct> storeProducts) {
        this.storeProducts = storeProducts;
    }
}
