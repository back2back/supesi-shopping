package co.supesi.shopping.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "orders",

        indices = {@Index(value = "product_id")}
)
public class Order {

    @PrimaryKey()
    @SerializedName("product_id")
    private Integer product_id;

    @ColumnInfo(name = "product_name")
    private String productName;

    @ColumnInfo(name = "qty")
    @SerializedName("qty")
    private Integer quantity;



    @ColumnInfo(name = "sell_at")
    private int price;

    @Ignore
    public Order(){

    }

    public Order(int product_id, String productName, int quantity, int price){
        this.product_id = product_id;
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;

    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
