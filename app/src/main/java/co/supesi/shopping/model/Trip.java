package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

public class Trip {
    @SerializedName("id")
    private String id;
    @SerializedName("nice_amount")
    private int niceAmount;
    @SerializedName("user")
    private User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNiceAmount() {
        return niceAmount;
    }

    public void setNiceAmount(int niceAmount) {
        this.niceAmount = niceAmount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
