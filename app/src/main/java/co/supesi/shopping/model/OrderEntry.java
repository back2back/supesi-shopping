package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderEntry {
    @SerializedName("id")
    private int id;
    @SerializedName("status")
    private int status;
    @SerializedName("store")
    private Store store;
    @SerializedName("nice_amount")
    private int total;
    @SerializedName("orders")
    private ArrayList<Order> orders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }
}
