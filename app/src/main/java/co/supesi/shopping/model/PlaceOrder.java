package co.supesi.shopping.model;

import com.google.gson.annotations.SerializedName;

public class PlaceOrder {
    @SerializedName("store_id")
    private String storeId;
    @SerializedName("end_lat")
    private String lat;
    @SerializedName("end_lng")
    private String lng;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("qty")
    private String qty;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
