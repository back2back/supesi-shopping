package co.supesi.shopping.utility;

public class Constants {
    private final static String PREF_JWT = "co.supesi.shopping.PREFERENCE_JWT";
    private final static String DEVICE_ID = "1000";

    public static String getDeviceId() {
        return DEVICE_ID;
    }

    public static String getPrefJwt() {
        return PREF_JWT;
    }
}
