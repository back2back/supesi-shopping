package co.supesi.shopping;

import android.app.Application;

import com.facebook.stetho.Stetho;

import im.crisp.sdk.Crisp;

public class BasicApp extends Application {
    private static BasicApp mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        if(BuildConfig.DEBUG){
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build());
        }
        Crisp.initialize(this);
        // Replace it with your WEBSITE_ID
        // Retrieve it using https://app.crisp.chat/website/[YOUR_WEBSITE_ID]/
        Crisp.getInstance().setWebsiteId(getString(R.string.crips_id));
    }

    public static BasicApp getContext(){return mContext;}
}
