package co.supesi.shopping;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.shopping.adapter.CheckoutItemsAdapter;
import co.supesi.shopping.database.DataRepository;
import co.supesi.shopping.model.ApiOrderPlacedResponse;
import co.supesi.shopping.model.Order;
import co.supesi.shopping.model.PlaceOrder;
import co.supesi.shopping.network.NetworkData;
import co.supesi.shopping.network.RetrofitClientInstance;
import co.supesi.shopping.ui.CartViewModel;
import co.supesi.shopping.ui.orders.OrderActivity;
import co.supesi.shopping.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {

    private static final String TAG = "CheckoutActivity";
    CartViewModel cartViewModel;
    private Unbinder unbinder;
    CheckoutItemsAdapter adapter;
    public final static String DELIVERY ="del";
    public final static String STORE_ID = "id";
    public final static String LAT = "lat";
    public final static String LNG = "lng";
    String delivery;
    String lat;
    String lng;
    String storeId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_checkout)
    RecyclerView recyclerView;
    @BindView(R.id.tv_payable_amount)
    TextView tvPayableAmount;
    @BindView(R.id.tv_del_amount)
    TextView tvDeliveryAmount;
    @BindView(R.id.btn_place_order)
    Button btnPlaceOrder;

    private SharedPreferences pref;
    private String jwt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        setUp();
        setAdapter();



    }

    private void placeOrder(String lat, String lng, String storeId) {
        cartViewModel.getOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(List<Order> orders) {
                if (orders.size() > 0){
                    readyPost(orders, lat, lng, storeId);
                }

            }
        });

        
    }

    private void readyPost(List<Order> orders, String lat, String lng, String storeId) {

        ArrayList<PlaceOrder> placeOrders = new ArrayList<>();
        for (Order order: orders){
            PlaceOrder placeOrder = new PlaceOrder();
            Integer quantity = order.getQuantity();
            int productId = order.getProduct_id();
            placeOrder.setQty(String.valueOf(quantity));
            placeOrder.setProductId(String.valueOf(productId));
            placeOrder.setLat(lat);
            placeOrder.setLng(lng);
            placeOrder.setStoreId(storeId);
            placeOrders.add(placeOrder);
        }

        pref = getApplication().getSharedPreferences(Constants.getPrefJwt(),MODE_PRIVATE);
        jwt = pref.getString("jwt", null);


        NetworkData networkData = RetrofitClientInstance.getRetrofitInstance().create(NetworkData.class);
        Call<ApiOrderPlacedResponse> call = networkData.placeOrder(jwt, placeOrders);
        call.enqueue(new Callback<ApiOrderPlacedResponse>() {
            @Override
            public void onResponse(Call<ApiOrderPlacedResponse> call, Response<ApiOrderPlacedResponse> response) {
                if (response.body() != null){
                    String msg = response.body().getMessage();
                    if (Boolean.parseBoolean(msg)){
                        Log.e(TAG, "order placed successful");
                        launchTrackingFrag();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiOrderPlacedResponse> call, Throwable t) {
                Log.e(TAG, "failed to place order " + t.getMessage());
            }
        });
    }

    private void launchTrackingFrag() {
        //clear cart
        DataRepository dataRepository = new DataRepository(BasicApp.getContext());
        dataRepository.deleteAllOrders();

        Intent intent = new Intent(this, OrderActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        lat = getIntent().getStringExtra(CheckoutActivity.LAT);
        lng = getIntent().getStringExtra(CheckoutActivity.LNG);
        storeId = getIntent().getStringExtra(CheckoutActivity.STORE_ID);


        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeOrder(lat, lng, storeId);
            }
        });
    }

    private void setAdapter() {


        cartViewModel = new ViewModelProvider(this).get(CartViewModel.class);

        adapter = new CheckoutItemsAdapter(this, cartViewModel);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        cartViewModel.getOrders().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(List<Order> orders) {
                adapter.setOrders(orders);
            }
        });
        cartViewModel.getSumTotal().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer order) {
                if (order != null){
                    int total = order + Integer.parseInt(delivery);
                    tvPayableAmount.setText(String.valueOf(total));
                }else{
                    tvPayableAmount.setText("0");
                }


            }
        });

    }

    private void setUp() {
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.checkout_title);
        }
        delivery = getIntent().getStringExtra(CheckoutActivity.DELIVERY);
        tvDeliveryAmount.setText(delivery);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
