package co.supesi.shopping.network;

import java.util.ArrayList;

import co.supesi.shopping.model.ApiAppCategoriesResponse;
import co.supesi.shopping.model.ApiCurrentOrder;
import co.supesi.shopping.model.ApiOrderPlacedResponse;
import co.supesi.shopping.model.ApiStoreProductsResponse;
import co.supesi.shopping.model.ApiStoresResponse;
import co.supesi.shopping.model.ApiUserResponse;
import co.supesi.shopping.model.AppCategoriesResponse;
import co.supesi.shopping.model.FindStoreDelivery;
import co.supesi.shopping.model.GetGeocodedPlace;
import co.supesi.shopping.model.PlaceOrder;
import co.supesi.shopping.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NetworkData {
    @GET("stores.json")
    Call<ApiStoresResponse> getStores();

    @GET("app-categories.json")
    Call<ApiAppCategoriesResponse> getAppCategories(@Query("lat") double lat, @Query("lng") double lng);

    @POST("users/login.json")
    Call<ApiUserResponse> login(@Body User user);

    @GET("users/profile.json")
    Call<ApiUserResponse> getProfile(@Header("Authorization") String jwt);

    @POST("users/customer.json")
    Call<ApiUserResponse> addUser(@Body User user);

    //@GET("store-products/{storeId}.json")
    //Call<ApiStoreProductsResponse> getProducts(@Path("storeId") int storeId);

    @POST("store-products/delivery.json")
    Call<ApiStoreProductsResponse> getProducts(@Body FindStoreDelivery findStoreDelivery);

    @POST("orders.json")
    Call<ApiOrderPlacedResponse> placeOrder(@Header("Authorization") String jwt, @Body ArrayList<PlaceOrder> placeOrder);

    @GET("order-entries.json")
    Call<ApiCurrentOrder> getCurrentOrder(@Header("Authorization") String jwt);

    @GET("https://maps.googleapis.com/maps/api/geocode/json")
    Call<GetGeocodedPlace> mGetGeocodedPlace(@Query("latlng") String latlng,
                                             @Query("result_type") String resultType,
                                             @Query("key") String apiKey);
}
