package co.supesi.shopping.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import co.supesi.shopping.model.Order;

@Dao
public interface OrderDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Order...orders);

    @Query("SELECT * from orders")
    LiveData<List<Order>> getOrders();

    @Query("DELETE from orders")
    void deleteAll();

    @Query("DELETE FROM orders WHERE product_id = :productId")
    void deleteItem(int productId);

    @Query("UPDATE orders SET qty =:quantitySet WHERE product_id =:productId")
    void  getUpdatedOrder(int productId, int quantitySet);


    @Query("SELECT SUM(qty * sell_at) FROM ORDERS")
    LiveData <Integer> getOrderTotal();
}
