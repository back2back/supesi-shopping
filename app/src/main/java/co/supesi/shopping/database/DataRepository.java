package co.supesi.shopping.database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import co.supesi.shopping.model.FindStoreDelivery;
import co.supesi.shopping.model.Order;
import co.supesi.shopping.model.User;
import co.supesi.shopping.utility.Constants;
import retrofit2.http.PUT;

public class DataRepository {
    //orders
    OrderDao orderDao;
    LiveData<List<Order>> allOrders;
    LiveData<Integer> totals;

    //user
    UserDao userDao;
    LiveData<User> user;

    //location
    LocationDao locationDao;
    LiveData<FindStoreDelivery> latLng;

    AppDatabase db;

    public DataRepository(Application application){
        db = AppDatabase.getAppDatabase(application);

        orderDao = db.orderDao();
        allOrders = orderDao.getOrders();
        totals = orderDao.getOrderTotal();

        userDao = db.userDao();
        user = userDao.getUser();

        locationDao = db.locationDao();
        latLng = locationDao.getLatLng(Constants.getDeviceId());
    }

    public LiveData<List<Order>> getAllOrders(){
        return allOrders;
    }

    public LiveData<Integer> getTotals(){
        return totals;
    }

    public void insertOrder(Order order){
        AppDatabase.databaseWriteExecutor.execute(()-> {
            orderDao.insert(order);
        });
    }

    public void updateOrder(Order order){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            //if order is less than 1, delete
            int qty = order.getQuantity();
            if (qty < 1){
                orderDao.deleteItem(order.getProduct_id());
            }else {
                orderDao.getUpdatedOrder(order.getProduct_id(),qty);
            }


        });
    }
    public void deleteAllOrders(){
        AppDatabase.databaseWriteExecutor.execute(()-> {
            orderDao.deleteAll();
        });
    }

    //location
    public void insertLocation(FindStoreDelivery findStoreDelivery){
        AppDatabase.databaseWriteExecutor.execute(()->{
            locationDao.insert(findStoreDelivery);
        });
    }


    public LiveData<FindStoreDelivery> getLatLng(){
        if (latLng == null){
            latLng = new MutableLiveData<>();
        }
        return latLng;
    }

    //user
    public void insertUser(User user){
        AppDatabase.databaseWriteExecutor.execute(()->{
            userDao.insert(user);
        });
    }


    public LiveData<User> getUser(){
        if (user == null){
            user = new MutableLiveData<>();
        }
        return  user;
    }
    public User getSingleUser(){
        return userDao.getSingleUser();
    }
}
