package co.supesi.shopping.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import co.supesi.shopping.model.FindStoreDelivery;
import co.supesi.shopping.model.Order;
import co.supesi.shopping.model.User;

@Database(entities = { Order.class, User.class, FindStoreDelivery.class}, version = 8)
public abstract class AppDatabase extends RoomDatabase {

    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public abstract OrderDao orderDao();
    public abstract UserDao userDao();
    public abstract LocationDao locationDao();

    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(Context context){
        if (INSTANCE == null){
            synchronized (AppDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class, "supesi")
                            .addCallback(mCallback)
                            .fallbackToDestructiveMigration()
                            //.allowMainThreadQueries()
                            .build();
                }
            }

        }
        return INSTANCE;
    }
    //for demo, every time we open the connection delete what was there previously
    private static RoomDatabase.Callback mCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            databaseWriteExecutor.execute(()->{
                //OrderDao orderDao = INSTANCE.orderDao();
                //orderDao.deleteAll();

            });
        }
    };
}
