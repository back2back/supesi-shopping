package co.supesi.shopping.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import co.supesi.shopping.model.FindStoreDelivery;

@Dao
public interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FindStoreDelivery... findStoreDeliveries);

    @Query("SELECT * FROM FindStoreDelivery  WHERE device_id = :device_id")
    LiveData<FindStoreDelivery> getLatLng(String device_id);


}
